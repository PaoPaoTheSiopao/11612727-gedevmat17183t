﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using GDEVMAT_17183T.Models;
using GDEVMAT_17183T.Utilities;

namespace GDEVMAT_17183T
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Vector3 mousePosition = new Vector3();
        List<Circle> myCircle = new List<Circle>();
        List<Cube> newCube = new List<Cube>();
        public Cube massivePlanet = new Cube()
        {
            Mass = 5,
            Scale = new Vector3(5, 5, 5)
        };
        private Cube smallPlanet = new Cube()
        {
            Mass = 1,
            Position = new Vector3(15, 20, 0)
        };
        private int frames = 0;
        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "GDEVMAY DAY 9";
            OpenGL gl = args.OpenGL;

            //Clear the screen and the depth buffer
            gl.Clear(OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -100.0f);

            //massivePlanet.Render(gl);
            //smallPlanet.Render(gl);

            //smallPlanet.ApplyForce(massivePlanet.CalculateAttraction(smallPlanet));
            if (frames <= 10)
            {
                newCube.Add(new Cube()
                {
                    color=new Models.Color(RandomNumberGenerator.GenerateDouble(0,1), RandomNumberGenerator.GenerateDouble(0, 1), RandomNumberGenerator.GenerateDouble(0, 1)),
                    Mass = RandomNumberGenerator.GenerateInt(1,5),
      
                    Position = new Vector3(RandomNumberGenerator.GenerateInt(-40, 40), RandomNumberGenerator.GenerateInt(-40, 40), 0)
                });
                myCircle.Add(new Circle()
                {
                    color = new Models.Color(RandomNumberGenerator.GenerateDouble(0, 1), RandomNumberGenerator.GenerateDouble(0, 1), RandomNumberGenerator.GenerateDouble(0, 1)),
                    Mass =RandomNumberGenerator.GenerateInt(1,5),
                    Position= new Vector3(RandomNumberGenerator.GenerateInt(-40, 40), RandomNumberGenerator.GenerateInt(-40, 40), 0)
                });
            }
            foreach(var i in newCube)
            {
                i.Render(gl);
                i.Scale = new Vector3(i.Mass / 2, i.Mass / 2, i.Mass / 2);
                i.ApplyForce(massivePlanet.CalculateAttraction(i));
                foreach(var c in myCircle)
                {
                    c.ApplyForce(i.CalculateAttraction(i));
                }
                foreach(var b in newCube)
                {
                    b.ApplyForce(i.CalculateAttraction(i));
                }
            }
            foreach(var z in myCircle)
            {
                z.Render(gl);
                z.ApplyForce(massivePlanet.CalculateAttraction(z));
                foreach(var y in newCube)
                {
                    y.ApplyForce(z.CalculateAttraction(y));
                }
                foreach(var x in myCircle)
                {
                    x.ApplyForce(z.CalculateAttraction(x));
                }
            }
            frames++;
        }
        #region INITIALIZATION
        public MainWindow()
        {
            InitializeComponent();
        }
        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 0.5f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 0.1f };
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_CONSTANT_ALPHA_EXT);
            gl.Enable(OpenGL.GL_BLEND);
            gl.ClearColor(0, 0, 0, 0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }
        #endregion

        private void OpenGLControl_MouseMove(object sender, MouseEventArgs e)
        {
            var position = e.GetPosition(this);
            mousePosition.x = (float)position.X - (float)Width / 2.0f;
            mousePosition.y = -((float)position.Y - (float)Height / 2.0f);
            mousePosition.Normalize();
            mousePosition *= 6;
        }
    }
}
