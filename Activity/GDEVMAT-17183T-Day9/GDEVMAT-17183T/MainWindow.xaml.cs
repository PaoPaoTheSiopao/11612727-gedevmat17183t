﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using GDEVMAT_17183T.Models;
using GDEVMAT_17183T.Utilities;

namespace GDEVMAT_17183T
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Circle> circles = new List<Circle>();
        private List<Cube> cubes = new List<Cube>();
        private Vector3 mousePosition = new Vector3();

        private Vector3 wind = new Vector3(0.05f, 0, 0);
        private Vector3 gravity = new Vector3(0,-0.5f, 0);
        private Vector3 twister = new Vector3(0.001f, 0.001f, 0);
        private Vector3 helium = new Vector3(0.1f, 0.1f, 0);
 
        public int frames = 0;
        public int massCounter = 9;
       
        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "GDEVMAY DAY 6";
            OpenGL gl = args.OpenGL;

            //Clear the screen and the depth buffer
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -100.0f);
            /*
            if(frames%5==0)
            {
                cubes.Add(new Cube()
                {
                    red = (float)RandomNumberGenerator.GenerateDouble(0.1f, 1),
                    blue = (float)RandomNumberGenerator.GenerateDouble(0.1f, 1),
                    green = (float)RandomNumberGenerator.GenerateDouble(0.1f, 1),
                    Mass = (float)RandomNumberGenerator.GenerateInt(1, 10),
                    Position = new Vector3((float)RandomNumberGenerator.GenerateGaussian(0, 15), 30, 0)
                });
            }
            if(frames%10==0)
            {
                circles.Add(new Circle()
                {
                    red = (float)RandomNumberGenerator.GenerateDouble(0.1f, 1),
                    blue = (float)RandomNumberGenerator.GenerateDouble(0.1f, 1),
                    green = (float)RandomNumberGenerator.GenerateDouble(0.1f, 1),
                    alpha = (float)RandomNumberGenerator.GenerateDouble(0.1f, 1),
                    radius=(float)RandomNumberGenerator.GenerateInt(1,5),
                    Mass = (float)RandomNumberGenerator.GenerateInt(1, 10),
                    Position =new Vector3((float)RandomNumberGenerator.GenerateGaussian(0, 15), 30,0)
                });
            }
            */
            foreach (var c in circles)
            {
                c.Render(gl);
                //c.ApplyForce(gravity);
                c.ApplyGravity();
                c.ApplyForce(wind);

                var coefficient = 0.01f;
                var normal = 1;
                var frictionMagnitude = coefficient * normal;
                var friction = c.Velocity;
                friction *= -1;
                friction.Normalize();
                friction *= frictionMagnitude;

                c.ApplyForce(friction);
                if (c.Position.y <= -25)
                {
                    c.Position.y = -25;
                    c.Velocity.y *= -1;
                }
                if(c.Position.x>=35)
                {
                    c.Position.x = 35;
                    c.Velocity.x *= -1;
                }
            }
            /*
            foreach(var s in cubes)
            {
                s.Render(gl);
                s.ApplyForce(gravity);
                if(s.Position.y<=-25)
                {
                    s.Velocity.y *= -1;
                }
                s.ApplyForce(mousePosition * 0.08f);
            }
            
            if(frames%300==0)
            {
                circles.Clear();
                cubes.Clear();
            }
            */
            frames++;
            massCounter--;
            
            
        }
        #region INITIALIZATION
        public MainWindow()
        {
            InitializeComponent();
            for (int x = 1; x <= 10; x++)
            {
                circles.Add(new Circle()
                {
                    red = (float)RandomNumberGenerator.GenerateDouble(0.1f, 1),
                    blue = (float)RandomNumberGenerator.GenerateDouble(0.1f, 1),
                    green = (float)RandomNumberGenerator.GenerateDouble(0.1f, 1),
                    alpha = (float)RandomNumberGenerator.GenerateDouble(0.1f, 1),
                    radius = x,
                    Mass = x,
                    Position = new Vector3(-30, 30, 0),
                });
            }
        }
        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 0.5f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 0.1f };
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_CONSTANT_ALPHA_EXT);
            gl.Enable(OpenGL.GL_BLEND);
            gl.ClearColor(0, 0, 0, 0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }
        #endregion

        private void OpenGLControl_MouseMove(object sender, MouseEventArgs e)
        {
            var position = e.GetPosition(this);
            mousePosition.x = (float)position.X - (float)Width / 2.0f;
            mousePosition.y = -((float)position.Y - (float)Height / 2.0f);
            mousePosition.Normalize();
            mousePosition *= 6;
        }
    }
}
