﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDEVMAT_17183T.Models
{
    public class Color
    {
        public double red, green, blue;
        public double alpha = 1;

        public Color(double r=1,double b=1,double g=1)
        {
            this.red = r;
            this.blue = b;
            this.green = g;
        }
    }
}
