﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using GDEVMAT_17183T.Models;
using GDEVMAT_17183T.Utilities;

namespace GDEVMAT_17183T
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Vector3 mousePosition = new Vector3();
        List<Circle> myCircle = new List<Circle>();
        List<Cube> newCube = new List<Cube>();
        private int frames=0;
        private Cube turret = new Cube(5, -25, 5);
        turret.scale
        private Liquid Ocean = new Liquid(0, 0, 100, 50, 0.9f);
        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "GDEVMAY DAY 9";
            OpenGL gl = args.OpenGL;

            //Clear the screen and the depth buffer
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -100.0f);

            turret.Render(gl);
            Ocean.Render(gl);

            if (frames % 5 == 0)
            {
                newCube.Add(new Cube()
                {
                    Mass = (float)RandomNumberGenerator.GenerateInt(1, 10),
                    Position = new Vector3((float)RandomNumberGenerator.GenerateInt(-30, 30), 30, 0)
                });
            }

            
            foreach (var i in newCube)
            {
                i.Render(gl);
                i.ApplyGravity();
                i.Scale = new Vector3(i.Mass / 2, i.Mass / 2, i.Mass / 2);
                if (i.Position.y <= -40)
                {
                    i.Position.y = -40;
                    i.Velocity.y *= -1;
                }
                if (Ocean.ContainsMovable(i))
                {
                    var dragForce = Ocean.CalculateDragForce(i);
                    i.ApplyForce(dragForce);
                }
            }
           
            
            /*
            if(frames%100==0)
            {
                newCube.Clear();
            }
            */

         frames++;
        }
            #region INITIALIZATION
            public MainWindow()
        {
            InitializeComponent();
        }
        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 0.5f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 0.1f };
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_CONSTANT_ALPHA_EXT);
            gl.Enable(OpenGL.GL_BLEND);
            gl.ClearColor(0, 0, 0, 0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }
        #endregion

        private void OpenGLControl_MouseMove(object sender, MouseEventArgs e)
        {
            var position = e.GetPosition(this);
            mousePosition.x = (float)position.X - (float)Width / 2.0f;
            mousePosition.y = -((float)position.Y - (float)Height / 2.0f);
            mousePosition.Normalize();
            mousePosition *= 6;
        }
    }
}
