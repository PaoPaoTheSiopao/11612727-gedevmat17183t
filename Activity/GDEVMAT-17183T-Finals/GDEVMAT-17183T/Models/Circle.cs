﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDEVMAT_17183T.Models
{
    public class Circle : Movable
    {
        private const int TOTAL_CIRCLE_ANGLE = 360;

        public float radius = 1.0f;
        public Circle()
        {

        }
        public Circle(Vector3 initPos)
        {
            this.Position = initPos;
        }
        public override void Render(OpenGL gl)
        {
            gl.PointSize(2.0f);
            gl.Color(color.red, color.green, color.blue, color.alpha);
            gl.Begin(OpenGL.GL_LINES);
            for(int i=0;i<=TOTAL_CIRCLE_ANGLE;i++)
            {
                gl.Vertex(Position.x, Position.y);
                gl.Vertex(Math.Cos(i) * radius + Position.x, Math.Sin(i) * radius + Position.y);
            }
            gl.End();
            UpdateMotion();
        }
        private void UpdateMotion()
        {
            this.Velocity += this.Acceleration;
            this.Position += this.Velocity;
            this.Acceleration *= 0;
        }
        public bool IsCollided(Circle target)
        {
            float currentCircle = this.radius;
            float targetCircle = target.radius;
            float totalRadius = currentCircle + targetCircle;
            float currentX = this.Position.x;
            float currentY = this.Position.y;
            float targetX = target.Position.x;
            float targetY = target.Position.y;
            float totalX = (targetX - currentX) * (targetX - currentX);
            float totalY = (targetY - currentY) * (targetY - currentY);

            float totalDistance = (totalX+totalY)*(0.5f);

            if(totalDistance<=totalRadius)
            {
                return true;
            }

            else { return false; }
            
        }
    }
}
