﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using GDEVMAT_17183T.Models;
using GDEVMAT_17183T.Utilities;

namespace GDEVMAT_17183T
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Vector3 mousePosition = new Vector3();
        List<Circle> myCircle = new List<Circle>();
        List<Circle> myCircle2 = new List<Circle>();
        List<Cube> myCube = new List<Cube>();
        Circle player = new Circle()
        {
            Mass = 5,
            radius = 1.5f
        };
        Vector3 gravity = new Vector3(0, -0.8f, 0);
        Vector3 twister = new Vector3(0.1f, 0.1f, 0);
        Vector3 twister2 = new Vector3(-0.1f, -0.1f, 0);
        public int playerScore = 0;
        private int frames = 0;
        private int multiplier = 1;
        bool canStillPlay = true;
        private Liquid Ocean = new Liquid(0, 0, 100, 50, 0.9f);
        private Liquid water = new Liquid(0, 0, 200, 200, 0.9f);
        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "GDEVMAT FINALS";
            OpenGL gl = args.OpenGL;

            //Clear the screen and the depth buffer
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -100.0f);

            if (canStillPlay)
            {
                gl.DrawText(0, 0, 1, 1, 1, "Arial", 25, string.Format("AVOID the circles", playerScore));
                gl.DrawText(0, 40, 1, 1, 1, "Arial", 25, string.Format("Score : {0}", playerScore));
            }

            player.Render(gl);
            if(Keyboard.IsKeyDown(Key.W))
            {
                if (playerScore >= 20 && playerScore <= 29)
                {
                    if (water.ContainsMovable(player))
                    {
                        player.Position.y += 0.5f;
                    }
                }
                player.Position.y += 2;
            }
            if(Keyboard.IsKeyDown(Key.S))
            {
                if (playerScore >= 20 && playerScore <= 29)
                {
                    if (water.ContainsMovable(player))
                    {
                        player.Position.y -= 0.5f;
                    }
                }
                player.Position.y -= 2;
            }
            if(Keyboard.IsKeyDown(Key.A))
            {
                if (playerScore >= 20 && playerScore <= 29)
                {
                    if (water.ContainsMovable(player))
                    {
                        player.Position.x -= 0.5f;
                    }
                }
                player.Position.x -= 2;
            }
            if(Keyboard.IsKeyDown(Key.D))
            {
                if (playerScore >= 20 && playerScore <= 29)
                {
                    if (water.ContainsMovable(player))
                    {
                        player.Position.x += 0.5f;
                    }
                }
                player.Position.x += 2;
            }
            if(player.Position.x>=70)
            {
                player.Position.x = 70;
            }
            if (player.Position.x <= -70)
            {
                player.Position.x = -70;
            }
            if (player.Position.y >= 35)
            {
                player.Position.y = 35;
            }
            if (player.Position.y <= -35)
            {
                player.Position.y = -35;
            }

            if(!canStillPlay)
            {
                GameOver();
                gl.DrawText(20, 20, 1, 1, 1, "Arial", 55, string.Format("Game Over!!!", playerScore));
                gl.DrawText(150, 150, 1, 1, 1, "Arial", 25, string.Format("Final Score : {0}", playerScore));
            }
            //
            foreach(var i in myCircle)
            {
                i.Render(gl);
                if(player.IsCollided(i))
                {
                    i.radius = 0;
                    canStillPlay = false;
                }
                if (i.Position.x >= 70)
                {
                    i.Position.x = 70;
                    i.Velocity.x *= -1;
                }
                if (i.Position.x <= -70)
                {
                    i.Position.x = -70;
                    i.Velocity.x *= -1;
                }
                if (i.Position.y >= 35)
                {
                    i.Position.y = 35;
                    i.Velocity.y *= -1;
                }
                if (i.Position.y <= -35)
                {
                    i.Position.y = -35;
                    i.Velocity.y *= -1;
                }
                if(playerScore>=10&&playerScore<=19)
                {
                    i.ApplyForce(twister);
                    
                }
                if(playerScore>=20&&playerScore<=29)
                {
                    water.Render(gl);
                    if (water.ContainsMovable(i))
                    {
                        var dragforce = water.CalculateDragForce(i);
                        i.ApplyForce(dragforce);
                        i.ApplyForce(twister);
                    }

                }
                if(playerScore>=30)
                {
                    i.ApplyForce(player.CalculateAttraction(i));
                }
            }
            //
            foreach(var c in myCircle2)
            {
                c.Render(gl);
                if (player.IsCollided(c))
                {
                    c.radius = 0;
                    canStillPlay = false;
                }
                if (c.Position.x >= 70)
                {
                    c.Position.x = 70;
                    c.Velocity.x *= -1;
                }
                if (c.Position.x <= -70)
                {
                    c.Position.x = -70;
                    c.Velocity.x *= -1;
                }
                if (c.Position.y >= 35)
                {
                    c.Position.y = 35;
                    c.Velocity.y *= -1;
                }
                if (c.Position.y <= -35)
                {
                    c.Position.y = -35;
                    c.Velocity.y *= -1;
                }
                if (playerScore >= 10 && playerScore <= 19)
                {
                    c.ApplyForce(twister2);

                }
                if (playerScore >= 20 && playerScore <= 29)
                {
                    if (water.ContainsMovable(c))
                    {
                        var dragforce = water.CalculateDragForce(c);
                        c.ApplyForce(dragforce);
                        c.ApplyForce(twister2);
                    }
                }
                if (playerScore >= 30)
                {
                    c.ApplyForce(player.CalculateAttraction(c));
                }
            }
            if(frames%40 ==0&&canStillPlay)
            {
                myCircle.Add(new Circle()
                {
                    color= new Models.Color(RandomNumberGenerator.GenerateDouble(0, 1), RandomNumberGenerator.GenerateDouble(0, 1), RandomNumberGenerator.GenerateDouble(0, 1)),
                    radius = 1,
                    Position = new Vector3(RandomNumberGenerator.GenerateInt(-70, 70), RandomNumberGenerator.GenerateInt(-35, 35), 0),
                    Mass = 2

                });
                myCircle2.Add(new Circle()
                {
                    color= new Models.Color(RandomNumberGenerator.GenerateDouble(0, 1), RandomNumberGenerator.GenerateDouble(0, 1), RandomNumberGenerator.GenerateDouble(0, 1)),
                    radius = 1,
                    Position = new Vector3(RandomNumberGenerator.GenerateInt(-70, 70), RandomNumberGenerator.GenerateInt(-35, 35), 0),
                    Mass = 2

                });
                playerScore += 1;
            }
            if(frames%600 == 0)
            {
                myCircle.Clear();
                myCircle2.Clear();
            }
            if(frames%1800==0)
            {
                multiplier += 1;
            }
            
            frames++;
        }

        private void GameOver()
        {
            myCircle.Clear();
            myCircle2.Clear();
            
        }

        #region INITIALIZATION
        public MainWindow()
        {
            InitializeComponent();
        }
        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 0.5f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 0.1f };
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_CONSTANT_ALPHA_EXT);
            gl.Enable(OpenGL.GL_BLEND);
            gl.ClearColor(0, 0, 0, 0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }
        #endregion

        private void OpenGLControl_MouseMove(object sender, MouseEventArgs e)
        {
            var position = e.GetPosition(this);
            mousePosition.x = (float)position.X - (float)Width / 2.0f;
            mousePosition.y = -((float)position.Y - (float)Height / 2.0f);
            mousePosition.Normalize();
            mousePosition *= 6;
        }
    }
}
